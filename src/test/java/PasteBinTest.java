import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

public class PasteBinTest {
    private WebDriver driver;
    private String pasteCode = "git config --global user.name \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private String pasteTitle = "how to gain dominance among developers";

    @Before
    public void setUp(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
    }
    @Test
    public void createNewPaste(){
        driver.get("https://pastebin.com");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        //Code text area
        WebElement textArea = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//textarea[@id=\"postform-text\"]")));
        textArea.sendKeys(new String[]{pasteCode});

        //Syntax
        WebElement syntaxDropdown = driver.findElement(
                By.xpath("//span[@id=\"select2-postform-format-container\"]"));
        syntaxDropdown.click();
        WebElement syntaxOption = driver.findElement(
                By.xpath("//ul[@role=\"listbox\"]//li[text()=\"Bash\"]"));
        syntaxOption.click();

        //Expiration dropdown
        WebElement expirationDropdown = driver.findElement(
                By.xpath("//span[@id='select2-postform-expiration-container']"));
        expirationDropdown.click();
        WebElement expirationOption = driver.findElement(
                By.xpath("//ul[@role=\"listbox\"]//li[text()=\"10 Minutes\"]"));
        expirationOption.click();

        // Paste Name / Title:
        WebElement title = driver.findElement(
                By.xpath("//input[@id=\"postform-name\"]"));
        title.sendKeys(new String[]{pasteTitle});

        // Create New Paste button
        WebElement createButton = driver.findElement(
                By.xpath("//button[text()=\"Create New Paste\"]"));
        createButton.click();

        wait.until(ExpectedConditions.titleContains(pasteTitle));

        //check title
        assertEquals(pasteTitle + " - Pastebin.com", driver.getTitle());
        //check code
        WebElement codeText = driver.findElement(By.className("bash"));
        assertEquals(pasteCode, codeText.getText());
        //check syntax
        WebElement syntaxButton = driver.findElement(By.xpath("//div[@class=\"top-buttons\"]/div[@class=\"left\"]/a"));
        assertEquals("Bash", syntaxButton.getText());
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
